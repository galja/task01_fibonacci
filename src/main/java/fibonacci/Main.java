package fibonacci;

public final class Main {
    private Main(){}
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.inputBounds();
        calculator.printOddNumbers();
        calculator.printEvenNumbers();
        calculator.printSumOfOddAndEvenNumbers();
        calculator.buildFibonacci();

    }
}
