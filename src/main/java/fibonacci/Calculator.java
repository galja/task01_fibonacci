package fibonacci;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Halyna Huzar galjagv@gmail.com
 * @version 1.0, 10 March 2019
 */
public class Calculator {

    /**
     * upper bound of the interval
     */
    private int upperBound;
    /**
     * lower bound of the interval
     */
    private int lowerBound;

    /**
     * Sets the upper and lower bounds
     */
    public final void inputBounds() {
        System.out.println("Enter the interval, please");
        lowerBound = scanAnInteger("The lower bound:");
        upperBound = scanAnInteger("The upper bound:");
    }

    /**
     * Finds odd numbers
     * Prints all the odd numbers from start to the end of the interval
     */
    public final void printOddNumbers() {
        System.out.println("Odd numbers in the interval: ");
        for (int i = lowerBound; i <= upperBound; i++) {
            if ((i % 2) != 0) {
                System.out.println(i + "\t");
            }
        }
        System.out.println();
    }

    /**
     * Checks parity of numbers
     * Prints all the even numbers from end to start of the interval
     */
    public final void printEvenNumbers() {
        System.out.print("Even numbers in the interval: ");
        for (int i = upperBound; i >= lowerBound; i--) {
            if ((i % 2) == 0) {
                System.out.print(i + "\t");
            }
        }
        System.out.println();
    }

    /**
     * Calculates and prints the sum of odd and even numbers
     */
    public final void printSumOfOddAndEvenNumbers() {
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;
        for (int i = lowerBound; i <= upperBound; i++) {
            if ((i % 2) != 0) {
                sumOfOddNumbers += i;
            } else {
                sumOfEvenNumbers += i;
            }
        }
        System.out.println("Sum of odd numbers: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers: " + sumOfEvenNumbers);
    }

    /**
     * Builds Fibonacci numbers
     * Assigns the biggest odd number to f1 and the biggest even number - to f2
     * Sets size of Fibonacci sequence
     * prints Fibonacci numbers
     */
    public final void buildFibonacci() {
        int n;
        int f1;
        int f2;
        if ((upperBound % 2) != 0) {
            f1 = upperBound;
            f2 = upperBound - 1;
        } else {
            f2 = upperBound;
            f1 = upperBound - 1;
        }
        n = scanAnInteger("Enter the size of set, please");
        printFibonacciNumbers(f1, f2, n);
    }

    /**
     * Evaluates and prints Fibonacci numbers, calculates quantity of even and odd numbers
     *
     * @param feb1 first number of Fibonacci sequence
     * @param feb2 first number of Fibonacci sequence
     * @param n    size of the set
     */
    private void printFibonacciNumbers(int feb1, int feb2, int n) {
        int f1 = feb1;
        int f2 = feb2;
        int numberOfEvenNumbers = 1;
        int numberOfOddNumbers = 1;
        int f;
        System.out.print("F1 = " + f1 + "\tF2 = " + f2 + "\t");
        for (int i = 2; i < n; i++) {
            f = f1 + f2;
            if ((f % 2) == 0) {
                numberOfEvenNumbers++;
            } else {
                numberOfOddNumbers++;
            }
            System.out.print("F" + (i + 1) + " =" + f + "\t");
            f1 = f2;
            f2 = f;
        }
        System.out.println();
        printPercentageOfFibonacci(numberOfEvenNumbers, numberOfOddNumbers);
    }

    /**
     * Evaluates and prints percentage of odd and even numrers in the set
     *
     * @param even quantity of even numbers
     * @param odd  quantity of odd numbers
     */
    private void printPercentageOfFibonacci(int even, int odd) {
        final int total = 100;
        int sum = even + odd;
        double percentage;
        try {
            percentage = (even * total) / (double) sum;
            System.out.println("Percentage of even Fibonacci numbers: " + percentage);
            percentage = (odd * total) / (double) sum;
            System.out.println("Percentage of odd Fibonacci numbers: " + percentage);
        } catch (ArithmeticException e) {
            System.out.println("Division by zero");
        }
    }

    /**
     * Scanns and gets a number, checks input
     *
     * @param message String varieble to be printed
     * @return the scanned number
     */
    private int scanAnInteger(String message) {
        int number = 0;
        Scanner in = new Scanner(System.in);
        System.out.println(message);
        try {
            number = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input. Enter an integer, please");
        }
        return number;
    }
}
